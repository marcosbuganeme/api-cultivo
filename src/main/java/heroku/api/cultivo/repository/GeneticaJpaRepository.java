package heroku.api.cultivo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import heroku.api.cultivo.modelo.Genetica;
import heroku.api.cultivo.modelo.enumerable.Familia;

/**
 * @author marcos olavo
 */
public interface GeneticaJpaRepository extends JpaRepository<Genetica, Long> {

	Genetica findByNomeAndFamilia(String nome, Familia familia);
}