package heroku.api.cultivo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import heroku.api.cultivo.modelo.Usuario;

/**
 * @author marcos olavo
 */
public interface UsuarioJpaRepository extends JpaRepository<Usuario, Long> {

}