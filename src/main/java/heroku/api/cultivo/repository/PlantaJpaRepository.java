package heroku.api.cultivo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import heroku.api.cultivo.modelo.Planta;
import heroku.api.cultivo.modelo.enumerable.TipoCultivo;
import heroku.api.cultivo.modelo.enumerable.TipoSemente;

/**
 * @author marcos olavo
 */
public interface PlantaJpaRepository extends JpaRepository<Planta, Long> {

	Planta findByNomeIgnoreCaseAndTipoSementeAndTipoCultivo(String nome, TipoSemente tipoSemente, TipoCultivo tipoCultivo);
}