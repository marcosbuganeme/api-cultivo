package heroku.api.cultivo.service;

import static java.lang.String.format;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import heroku.api.cultivo.modelo.Planta;
import heroku.api.cultivo.modelo.enumerable.TipoCultivo;
import heroku.api.cultivo.modelo.enumerable.TipoSemente;
import heroku.api.cultivo.repository.PlantaJpaRepository;
import heroku.api.cultivo.service.exceptions.DuplicateResourceException;
import heroku.api.cultivo.service.exceptions.ResourceNotFoundException;

/**
 * @author marcos olavo
 */
public @Service class PlantaService {

	static final String NOT_FOUND = "Planta [ %s ] não foi encontrada";
	static final String DUPLICATE = "Planta [ %s - %s - %s ] já foi cadastrada";

	private final PlantaJpaRepository repository;

	public @Autowired PlantaService(PlantaJpaRepository repository) {
		this.repository = repository;
	}

	public @Transactional Planta save(Planta planta) {
		validate(planta);

		return repository.save(planta);
	}

	public Planta findById(Long id) {
		return repository
					.findById(id)
					.orElseThrow(() -> new ResourceNotFoundException(format(NOT_FOUND, id)));
	}

	public Page<Planta> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	private void validate(Planta planta) {
		String nome = planta.getNome();
		TipoSemente tipoSemente = planta.getTipoSemente();
		TipoCultivo tipoCultivo = planta.getTipoCultivo();

		Planta result = repository.findByNomeIgnoreCaseAndTipoSementeAndTipoCultivo(nome, tipoSemente, tipoCultivo);

		if (Objects.nonNull(result)) {
			throw new DuplicateResourceException(format(DUPLICATE, nome, tipoSemente, tipoCultivo));
		}
	}
}