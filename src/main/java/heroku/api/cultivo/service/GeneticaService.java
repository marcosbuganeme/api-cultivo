package heroku.api.cultivo.service;

import static java.lang.String.format;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import heroku.api.cultivo.modelo.Genetica;
import heroku.api.cultivo.modelo.enumerable.Familia;
import heroku.api.cultivo.repository.GeneticaJpaRepository;
import heroku.api.cultivo.service.exceptions.DuplicateResourceException;
import heroku.api.cultivo.service.exceptions.ResourceNotFoundException;

/**
 * @author marcos olavo
 */
public @Service class GeneticaService {

	static final String NOT_FOUND = "Genética [ %s ] não foi encontrada";
	static final String DUPLICATE = "Genética [ %s - %s ] já foi cadastrada";

	private final GeneticaJpaRepository repository;

	public @Autowired GeneticaService(GeneticaJpaRepository repository) {
		this.repository = repository;
	}

	public @Transactional Genetica save(Genetica genetica) {
		validate(genetica);

		return repository.save(genetica);
	}

	public Genetica findById(Long id) {
		return repository
					.findById(id)
					.orElseThrow(notFoundException(id));
	}

	public Page<Genetica> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	private void validate(Genetica genetica) {
		String nome = genetica.getNome();
		Familia familia = genetica.getFamilia();

		Genetica search = repository.findByNomeAndFamilia(nome, familia);

		Optional
			.ofNullable(search)
			.filter(Objects::nonNull)
			.orElseThrow(duplicateException(nome, familia));
	}

	private Supplier<? extends ResourceNotFoundException> notFoundException(Long id) {
		return () -> new ResourceNotFoundException(format(NOT_FOUND, id));
	}

	private Supplier<? extends DuplicateResourceException> duplicateException(String nome, Familia familia) {
		return () -> new DuplicateResourceException(format(DUPLICATE, nome, familia));
	}
}