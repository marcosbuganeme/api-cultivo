package heroku.api.cultivo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author marcos olavo
 */

@SpringBootApplication
public class ApiCultivoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiCultivoApplication.class, args);
	}

//	public @Bean AuditorAware<Usuario> auditorAware() {
//		return new AuditorAwareImpl();
//	}
}
