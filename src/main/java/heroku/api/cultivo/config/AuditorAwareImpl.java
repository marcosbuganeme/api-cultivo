package heroku.api.cultivo.config;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import heroku.api.cultivo.modelo.Usuario;

/**
 * @author marcos olavo
 */
public class AuditorAwareImpl implements AuditorAware<Usuario> {

	public @Override Optional<Usuario> getCurrentAuditor() {

		Usuario autenticado = (Usuario) SecurityContextHolder
												.getContext()
												.getAuthentication()
												.getPrincipal();

		return Optional
					.ofNullable(autenticado);
	}
}