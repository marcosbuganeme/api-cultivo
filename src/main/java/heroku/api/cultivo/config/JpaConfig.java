package heroku.api.cultivo.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author marcos olavo
 */

@Configuration
@EnableJpaAuditing
@EnableTransactionManagement
@EntityScan(basePackages = { "heroku.api.cultivo.modelo" })
@EnableJpaRepositories(basePackages = { "heroku.api.cultivo.repository" })
public class JpaConfig {}