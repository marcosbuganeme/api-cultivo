package heroku.api.cultivo.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import heroku.api.cultivo.modelo.Genetica;
import heroku.api.cultivo.resource.permissions.Cultivo;
import heroku.api.cultivo.service.GeneticaService;

@RestController
@RequestMapping("v1/geneticas")
public class GeneticaRestController {

	private final GeneticaService service;

	public @Autowired GeneticaRestController(GeneticaService service) {
		this.service = service;
	}

	@Cultivo
	@PostMapping
	public ResponseEntity<?> save(@Valid @RequestBody Genetica genetica) {
		Genetica newGenetica = service.save(genetica);

		return ResponseEntity.ok(newGenetica);
	}

	@Cultivo
	@GetMapping("{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		Genetica search = service.findById(id);

		return ResponseEntity.ok(search);
	}

	@Cultivo
	@GetMapping
	public ResponseEntity<?> findAll(@PageableDefault Pageable pageable) {
		Page<Genetica> resultados = service.findAll(pageable);

		return ResponseEntity.ok(resultados);
	}
}
