package heroku.api.cultivo.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import heroku.api.cultivo.modelo.Planta;
import heroku.api.cultivo.resource.permissions.Cultivo;
import heroku.api.cultivo.service.PlantaService;

@RestController
@RequestMapping("v1/plantas")
public class PlantaRestController {

	private final PlantaService service;

	public @Autowired PlantaRestController(PlantaService service) {
		this.service = service;
	}

	@Cultivo
	@GetMapping("{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		Planta search = service.findById(id);

		return ResponseEntity.ok(search);
	}

	@Cultivo
	@PostMapping
	public ResponseEntity<?> save(@Valid @RequestBody Planta planta) {
		Planta newPlant = service.save(planta);

		return ResponseEntity.ok(newPlant);
	}

	@Cultivo
	@GetMapping
	public ResponseEntity<?> findAll(@PageableDefault Pageable pageable) {
		Page<Planta> results = service.findAll(pageable);
		return ResponseEntity.ok(results);
	}
}