package heroku.api.cultivo.resource.permissions;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.security.access.prepost.PreAuthorize;

/**
 * @author marcos olavo
 */

@Documented
@Retention(value = RUNTIME)
@PreAuthorize("hasRole('VENDA')")
@Target(value = { TYPE, METHOD })
public @interface Venda {}