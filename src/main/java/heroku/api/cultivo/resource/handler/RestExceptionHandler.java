package heroku.api.cultivo.resource.handler;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import heroku.api.cultivo.resource.errors.ErrorDetails;
import heroku.api.cultivo.resource.errors.ValidationDetails;

/**
 * @author marcos olavo
 */
public @RestControllerAdvice class RestExceptionHandler extends ResponseEntityExceptionHandler {

	protected @Override ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, 
																			HttpHeaders headers, 
																			HttpStatus status, 
																			WebRequest request) {

        List<FieldError> errors = exception.getBindingResult().getFieldErrors();

        String fields = getFieldsErrors(errors);
        String fieldMessages = getFieldsMessage(errors);

        ValidationDetails details = ValidationDetails.Builder.newBuilder()
													        		.field(fields)
													        		.fieldMessage(fieldMessages)
        															.timestamp(new Date().getTime())
        															.developer(exception.getClass().getName())
						        									.httpStatus(HttpStatus.BAD_REQUEST.value())
						        									.title("Erro de validação a nível de campo")
						        									.detail("Erro de validação a nível de campo")
						        								.build();

		return ResponseEntity.badRequest().body(details);
	}

	protected @Override ResponseEntity<Object> handleExceptionInternal(Exception exception, 
															 Object body, 
															 HttpHeaders headers, 
															 HttpStatus status, 
															 WebRequest request) {

		ErrorDetails details = ErrorDetails.Builder.newBuilder()
														.httpStatus(status.value())
														.title("Erro interno na API")
														.detail(exception.getMessage())
														.timestamp(new Date().getTime())
														.developer(exception.getClass().getName())
													.build();

		return new ResponseEntity<>(details, headers, status);
	}

	private String getFieldsMessage(List<FieldError> errors) {

		return errors
				.stream()
				.map(FieldError::getDefaultMessage)
				.collect(Collectors.joining(","));
	}

	private String getFieldsErrors(List<FieldError> errors) {

		return errors
				.stream()
				.map(FieldError::getField)
				.collect(Collectors.joining(","));
	}
}