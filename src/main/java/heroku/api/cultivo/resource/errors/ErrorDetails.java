package heroku.api.cultivo.resource.errors;

/**
 * @author marcos olavo
 */
public class ErrorDetails {

	private String titulo;
	private int httpStatus;
	private String detalhe;
	private long timestamp;
	private String desenvolvedor;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getDetalhe() {
		return detalhe;
	}

	public void setDetalhe(String detalhe) {
		this.detalhe = detalhe;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getDesenvolvedor() {
		return desenvolvedor;
	}

	public void setDesenvolvedor(String desenvolvedor) {
		this.desenvolvedor = desenvolvedor;
	}

	/**
	 * @author marcos olavo
	 */
	public static final class Builder {

		private String titulo;
		private int httpStatus;
		private String detalhe;
		private long timestamp;
		private String desenvolvedor;

		public static Builder newBuilder() {
			return new Builder();
		}

		public Builder title(String titulo) {
			this.titulo = titulo;
			return this;
		}

		public Builder httpStatus(int httpStatus) {
			this.httpStatus = httpStatus;
			return this;
		}

		public Builder detail(String detalhe) {
			this.detalhe = detalhe;
			return this;
		}

		public Builder timestamp(long timestamp) {
			this.timestamp = timestamp;
			return this;
		}

		public Builder developer(String desenvolvedor) {
			this.desenvolvedor = desenvolvedor;
			return this;
		}

		public ErrorDetails build() {
			ErrorDetails errorDetails = new ErrorDetails();
			errorDetails.titulo = titulo;
			errorDetails.detalhe = detalhe;
			errorDetails.timestamp = timestamp;
			errorDetails.httpStatus = httpStatus;
			errorDetails.desenvolvedor = desenvolvedor;
			return errorDetails;
		}
	}
}