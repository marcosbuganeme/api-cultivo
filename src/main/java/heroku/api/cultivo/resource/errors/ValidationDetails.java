package heroku.api.cultivo.resource.errors;

/**
 * @author marcos olavo
 */
public final class ValidationDetails extends ErrorDetails {

	private String field;
	private String fieldMessage;

	/**
	 * @author marcos olavo
	 */
	public static final class Builder {

		private String title;
		private int httpStatus;
		private String detail;
		private long timestamp;
		private String developer;
		private String field;
		private String fieldMessage;

		public static Builder newBuilder() {
			return new Builder();
		}

		public Builder title(String title) {
			this.title = title;
			return this;
		}

		public Builder httpStatus(int httpStatus) {
			this.httpStatus = httpStatus;
			return this;
		}

		public Builder detail(String detail) {
			this.detail = detail;
			return this;
		}

		public Builder timestamp(long timestamp) {
			this.timestamp = timestamp;
			return this;
		}

		public Builder developer(String developer) {
			this.developer = developer;
			return this;
		}

		public Builder field(String field) {
			this.field = field;
			return this;
		}

		public Builder fieldMessage(String fieldMessage) {
			this.fieldMessage = fieldMessage;
			return this;
		}

		public ValidationDetails build() {
			ValidationDetails validations = new ValidationDetails();
			validations.setTitulo(title);
			validations.setDetalhe(detail);
			validations.setTimestamp(timestamp);
			validations.setHttpStatus(httpStatus);
			validations.setDesenvolvedor(developer);
			validations.field = field;
			validations.fieldMessage = fieldMessage;

			return validations;
		}
	}

	public String getField() {
		return field;
	}

	public String getFieldMessage() {
		return fieldMessage;
	}
}