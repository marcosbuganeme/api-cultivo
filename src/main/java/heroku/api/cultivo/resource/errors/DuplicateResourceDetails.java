package heroku.api.cultivo.resource.errors;

/**
 * @author marcos olavo
 */
public final class DuplicateResourceDetails extends ErrorDetails {

	/**
	 * @author marcos olavo
	 */
	public static final class Builder {

		private String titulo;
		private int httpStatus;
		private String detalhe;
		private long timestamp;
		private String desenvolvedor;

		private Builder() {
			super();
		}

		public static Builder newBuilder() {
			return new Builder();
		}

		public Builder titulo(String titulo) {
			this.titulo = titulo;
			return this;
		}

		public Builder httpStatus(int httpStatus) {
			this.httpStatus = httpStatus;
			return this;
		}

		public Builder detalhe(String detalhe) {
			this.detalhe = detalhe;
			return this;
		}

		public Builder timestamp(long timestamp) {
			this.timestamp = timestamp;
			return this;
		}

		public Builder desenvolvedor(String desenvolvedor) {
			this.desenvolvedor = desenvolvedor;
			return this;
		}

		public ErrorDetails build() {
			DuplicateResourceDetails duplicateResource = new DuplicateResourceDetails();
			duplicateResource.setTitulo(titulo);
			duplicateResource.setDetalhe(detalhe);
			duplicateResource.setTimestamp(timestamp);
			duplicateResource.setHttpStatus(httpStatus);
			duplicateResource.setDesenvolvedor(desenvolvedor);
			return duplicateResource;
		}
	}
}