package heroku.api.cultivo.resource.errors;

/**
 * @author marcos olavo
 */
public class ResourceNotFoundDetails extends ErrorDetails {

	/**
	 * @author marcos olavo
	 */
	public static final class Builder {

		private String titulo;
		private int httpStatus;
		private String detalhe;
		private long timestamp;
		private String desenvolvedor;

		private Builder() {
		}

		public static Builder newInstance() {
			return new Builder();
		}

		public Builder titulo(String titulo) {
			this.titulo = titulo;
			return this;
		}

		public Builder httpStatus(int httpStatus) {
			this.httpStatus = httpStatus;
			return this;
		}

		public Builder detalhe(String detalhe) {
			this.detalhe = detalhe;
			return this;
		}

		public Builder timestamp(long timestamp) {
			this.timestamp = timestamp;
			return this;
		}

		public Builder desenvolvedor(String desenvolvedor) {
			this.desenvolvedor = desenvolvedor;
			return this;
		}

		public ErrorDetails build() {
			ResourceNotFoundDetails resourceNotFound = new ResourceNotFoundDetails();
			resourceNotFound.setTitulo(titulo);
			resourceNotFound.setDetalhe(detalhe);
			resourceNotFound.setTimestamp(timestamp);
			resourceNotFound.setHttpStatus(httpStatus);
			resourceNotFound.setDesenvolvedor(desenvolvedor);
			return resourceNotFound;
		}
	}
}
