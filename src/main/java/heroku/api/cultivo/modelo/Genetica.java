package heroku.api.cultivo.modelo;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.cache.annotation.Cacheable;

import heroku.api.cultivo.modelo.enumerable.Familia;
import heroku.api.cultivo.modelo.shared.AbstractEntity;

/**
 * @author marcos olavo
 */

@Entity
@Cacheable
@Table(name = "geneticas")
public final class Genetica extends AbstractEntity {

	private String nome;
	private Familia familia;
	private boolean medicinal;
	private BigDecimal indiceTHC;
	private List<Planta> plantas;

	{
		plantas = new LinkedList<>();
	}

	private @PrePersist void trigger() {
		atribuirFamiliaPadrao();
	}

	@Column(length = 100, nullable = false)
	@NotBlank(message = "[ Nome ] é obrigatório")
	@Size(min = 5, max = 100, message = "[ NOME ] deve conter entre no mínimo {min} e máximo {max} caracteres")
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Enumerated(EnumType.STRING)
	@Column(length = 20, nullable = false)
	@NotNull(message = "[ Família ] é obrigatório")
	public Familia getFamilia() {
		return familia;
	}

	public void setFamilia(Familia familia) {
		this.familia = familia;
	}

	@Column(nullable = false)
	public boolean isMedicinal() {
		return medicinal;
	}

	public void setMedicinal(boolean medicinal) {
		this.medicinal = medicinal;
	}

	@Column(name = "indice_thc", nullable = false)
	@NotNull(message = "[ ÍNDICE THC ] é obrigatório")
	@DecimalMax(message = "[ Índice de THC ] de nenhuma de nossas variedades é superior á 30%", value = "30.0")
	@DecimalMin(message = "[ Índice de THC ] de nenhuma de nossas variedades é inferior á 10%", value = "10.0")
	public BigDecimal getIndiceTHC() {
		return indiceTHC;
	}

	public void setIndiceTHC(BigDecimal indiceTHC) {
		this.indiceTHC = indiceTHC;
	}

	@OneToMany(mappedBy = "genetica")
	public List<Planta> getPlantas() {
		return Collections.unmodifiableList(plantas);
	}

	public void setPlantas(List<Planta> plantas) {
		this.plantas.clear();
		this.plantas = plantas;
	}

	private void atribuirFamiliaPadrao() {
		familia = Optional
					.ofNullable(familia)
					.filter(Objects::nonNull)
					.orElse(Familia.NAO_INFORMADO);
	}

	public @Override String toString() {
		return String.format("{ nome: %s, familia: %s, medicinal: %s, indiceTHC: %s, plantas: %s }",
				nome, familia, medicinal, indiceTHC, plantas);
	}
}