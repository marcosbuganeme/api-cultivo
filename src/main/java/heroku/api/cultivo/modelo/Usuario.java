package heroku.api.cultivo.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author marcos olavo
 */

@Entity
@Table(name = "usuarios")
public final class Usuario {

	private Integer id;
	private String nome;
	private String login;
	private String senha;
	private String email;
	private @Transient String contraSenha;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, unique = true, updatable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length = 100, nullable = false)
	@NotBlank(message = "[ Nome ] é obrigatório")
	@Size(min = 5, max = 100, message = "[ Nome ] deve conter no mínimo {min} e no máximo {max} caracteres")
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@NotBlank(message = "[ Login ] é obrigatório")
	@Column(length = 20, nullable = false, updatable = false)
	@Size(min = 5, max = 20, message = "[ Login ] deve conter no mínimo {min} e no máximo {max} caracteres")
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Column(length = 80, nullable = false)
	@NotBlank(message = "[ Senha ] é obrigatório")
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Column(length = 100, nullable = false)
	@NotBlank(message = "[ E-mail ] é obrigatório")
	@Size(min = 5, max = 100, message = "[ E-mail ] informado parece estar inválido")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContraSenha() {
		return contraSenha;
	}

	public void setContraSenha(String contraSenha) {
		this.contraSenha = contraSenha;
	}

	public @Override String toString() {
		return String.format("{ nome: %s, login: %s, senha: %s, email: %s }", nome, login, senha, email);
	}
}