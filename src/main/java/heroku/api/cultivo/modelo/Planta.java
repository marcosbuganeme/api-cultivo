package heroku.api.cultivo.modelo;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import heroku.api.cultivo.modelo.enumerable.TipoCultivo;
import heroku.api.cultivo.modelo.enumerable.TipoSemente;
import heroku.api.cultivo.modelo.shared.AbstractEntity;

/**
 * @author marcos olavo
 */

@Entity
@Cacheable
@Table(name = "plantas")
public final class Planta extends AbstractEntity {

	private String nome;
	private Genetica genetica;
	private TipoSemente tipoSemente;
	private TipoCultivo tipoCultivo;

	@Column(nullable = false, length = 100)
	@NotBlank(message = "[ Nome ] é obrigatório")
	@Size(min = 1, max = 100, message = "[ NOME ] deve conter entre no mínimo {min} e no máximo {max} caracteres")
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@ManyToOne
	@JoinColumn(name = "id_genetica")
	public Genetica getGenetica() {
		return genetica;
	}

	public void setGenetica(Genetica genetica) {
		this.genetica = genetica;
	}

	@Enumerated(EnumType.STRING)
	@NotNull(message = "[ Tipo da Semente ] é obrigatório")
	@Column(length = 10, name = "tipo_semente", nullable = false)
	public TipoSemente getTipoSemente() {
		return tipoSemente;
	}

	public void setTipoSemente(TipoSemente tipoSemente) {
		this.tipoSemente = tipoSemente;
	}

	@Enumerated(EnumType.STRING)
	@NotNull(message = "[ Tipo do Cultivo ] é obrigatório")
	@Column(length = 10, name = "tipo_cultivo", nullable = false)
	public TipoCultivo getTipoCultivo() {
		return tipoCultivo;
	}

	public void setTipoCultivo(TipoCultivo tipoCultivo) {
		this.tipoCultivo = tipoCultivo;
	}

	public @Override String toString() {
		return String.format("{ nome: %s, genetica: %s, tipoSemente: %s, tipoCultivo: %s }", nome, tipoSemente, genetica, tipoCultivo);
	}
}