package heroku.api.cultivo.modelo;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import heroku.api.cultivo.modelo.shared.AbstractEntity;

/**
 * @author marcos olavo
 */

@Entity
@Table(name = "itens_alocados")
public final class ItemAlocado extends AbstractEntity {

	private Estufa estufa;
	private Equipamento equipamento;

	@ManyToOne
	@JoinColumn(name = "id_estufa", nullable = false)
	public Estufa getEstufa() {
		return estufa;
	}

	public void setEstufa(Estufa estufa) {
		this.estufa = estufa;
	}

	@ManyToOne
	@JoinColumn(name = "id_equipamento", nullable = false)
	public Equipamento getEquipamento() {
		return equipamento;
	}

	public void setEquipamento(Equipamento equipamento) {
		this.equipamento = equipamento;
	}

	public @Override String toString() {
		return String.format("{ estufa: %s, equipamento: %s }", estufa, equipamento);
	}
}