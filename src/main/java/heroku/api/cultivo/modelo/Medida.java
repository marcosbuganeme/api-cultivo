package heroku.api.cultivo.modelo;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;

/**
 * Representa os números de medida de uma estufa
 * 
 * @author marcos olavo
 */
public @Embeddable final class Medida {

	private BigDecimal altura;
	private BigDecimal largura;
	private BigDecimal comprimento;

	@Column(nullable = false)
	@DecimalMax(value = "2.0", message = "[ Altura ] deve ser menor que 2m [ 200cm ]")
	@DecimalMin(value = "0.80", message = "[ Altura ] deve ser maior que 0.8m [ 80 cm ]")
	public BigDecimal getAltura() {
		return altura;
	}

	public void setAltura(BigDecimal altura) {
		this.altura = altura;
	}

	@Column(nullable = false)
	@DecimalMax(value = "1.4", message = "[ Largura ] deve ser menor que 1.4m [ 140cm ]")
	@DecimalMin(value = "0.80", message = "[ Largura ] deve ser maior que 0.8m [ 80 cm ]")
	public BigDecimal getLargura() {
		return largura;
	}

	public void setLargura(BigDecimal largura) {
		this.largura = largura;
	}

	@Column(nullable = false)
	@DecimalMax(value = "1.4", message = "[ Comprimento ] deve ser menor que 1.4m [ 140cm ]")
	@DecimalMin(value = "0.80", message = "[ Comprimento ] deve ser maior que 0.8m [ 80 cm ]")
	public BigDecimal getComprimento() {
		return comprimento;
	}

	public void setComprimento(BigDecimal comprimento) {
		this.comprimento = comprimento;
	}

	public @Override String toString() {
		return String.format("{ altura: %s, largura: %s, comprimento: %s }", altura, largura, comprimento);
	}
}