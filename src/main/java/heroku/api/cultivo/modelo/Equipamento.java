package heroku.api.cultivo.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import heroku.api.cultivo.modelo.shared.AbstractEntity;

/**
 * Representa um equipamento eletrônico que será alocado em uma estufa
 * @author marcos olavo
 */

@Entity
@Table(name = "equipamentos")
public final class Equipamento extends AbstractEntity {

	private String nome;

	@Column(length = 50, nullable = false)
	@NotBlank(message = "[ Nome ] é obrigatório")
	@Size(min = 5, max = 50, message = "[ Nome ] deve conter no mínimo {min} e no máximo {max} caracteres")
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public @Override String toString() {
		return String.format("{ nome: %s }", nome);
	}
}