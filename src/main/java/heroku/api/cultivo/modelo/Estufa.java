package heroku.api.cultivo.modelo;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import heroku.api.cultivo.modelo.shared.AbstractEntity;

/**
 * Representa a cabine de cultivo indoor
 * 
 * @author marcos olavo
 */

@Entity
@Table(name = "estufas")
public final class Estufa extends AbstractEntity {

	private Medida medida;
	private List<ItemAlocado> itens;

	{
		itens = new LinkedList<>();
	}

	@Embedded
	public Medida getMedida() {
		return medida;
	}

	public void setMedida(Medida medida) {
		this.medida = medida;
	}

	@OneToMany
	public List<ItemAlocado> getItens() {
		return itens;
	}

	public void setItens(List<ItemAlocado> itens) {
		this.itens = itens;
	}

	public @Override String toString() {
		return String.format("{ medida: %s, itens: %s }", medida, itens);
	}
}