package heroku.api.cultivo.modelo.enumerable;

/**
 * @author marcos olavo
 */
public enum TipoCultivo {

	INDOOR(1, "Indoor"),

	OUTDOOR(2, "Outdoor");

	final int valor;
	final String descricao;

	TipoCultivo(final int valor, final String descricao) {
		this.valor = valor;
		this.descricao = descricao;
	}

	public int getValor() {
		return valor;
	}

	public String getDescricao() {
		return descricao;
	}
}