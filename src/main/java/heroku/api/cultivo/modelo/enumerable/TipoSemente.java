package heroku.api.cultivo.modelo.enumerable;

/**
 * @author marcos olavo
 */
public enum TipoSemente {

	REGULAR(	1, "Regular"),

	FEMINIZADA(	2, "Feminizada");

	final int valor;
	final String descricao;

	TipoSemente(final int valor, final String descricao) {
		this.valor = valor;
		this.descricao = descricao;
	}

	public int getValor() {
		return valor;
	}

	public String getDescricao() {
		return descricao;
	}
}