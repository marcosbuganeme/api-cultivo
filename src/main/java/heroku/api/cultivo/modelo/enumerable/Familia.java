package heroku.api.cultivo.modelo.enumerable;

/**
 * @author marcos olavo
 */
public enum Familia {

	INDICA(1, "Indica"),

	SATIVA(2, "Sativa"),

	RUDERALIS(3, "Ruderalis"),

	MAIORIA_INDICA(4, "Maioria índica"),

	MAIORIA_SATIVA(5, "Maioria sativa"),

	NAO_INFORMADO(6, "Não informado");

	final int valor;
	final String descricao;

	Familia(final int valor, final String descricao) {
		this.valor = valor;
		this.descricao = descricao;
	}

	public int getValor() {
		return valor;
	}

	public String getDescricao() {
		return descricao;
	}
}